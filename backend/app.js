const express = require('express');
const axios = require('axios');
const router = express.Router();
require('dotenv').config();


const app = express();

app.get('/', (req, res) => {
    res.send('WE ARE ON HOME');
});

app.get('/api/news', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept");
    res.header("Access-Control-Allow-Methods", "PUT,GET,POST,DELETE,OPTIONS");

    axios.get('https://newsapi.org/v2/top-headlines?country=ph&apiKey=ed0500d6e8634921870ed08dd6baaa50')
        .then(function (response) {
            // handle success
            res.status(200).send({
                success: 'true',
                articles: response.data.articles
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept");
    res.header("Access-Control-Allow-Methods", "PUT,GET,POST,DELETE,OPTIONS");
});

module.exports = router;
app.listen(process.env.PORT, () => console.log('listening to port ' + process.env.PORT));
